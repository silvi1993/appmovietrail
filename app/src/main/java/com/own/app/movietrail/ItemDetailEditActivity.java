package com.own.app.movietrail;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ItemDetailEditActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseReference;
    private TextInputEditText title;
    private TextInputEditText image;
    private TextInputEditText description;
    private TextInputEditText erfasser;
    private RatingBar stars;
    private String email;
    private String bewertungsuser;
    private String userID;
    private Button bSubmit;
    private float starsaverage = 0;
    private int starscount = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail_edit);
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();
        bewertungsuser = user.getEmail();
        title = (TextInputEditText) findViewById(R.id.tiet_movie_name);
        image = (TextInputEditText) findViewById(R.id.tiet_movie_logo);
        description = (TextInputEditText) findViewById(R.id.tiet_movie_description);
        //erfasser = (TextInputEditText) findViewById(R.id.tiet_movie_erfasser);
        bSubmit = (Button) findViewById(R.id.b_submit);
        stars = (RatingBar) findViewById(R.id.rating_bar);
        //initialice Database
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isEmpty(title) && !isEmpty(image) && !isEmpty(description)){
                    SimpleDateFormat datumsformat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                    String datum = datumsformat.format(new Date());
                    myNewMovie(title.getText().toString().trim(),  cutyoutubestring(image.getText().toString().trim())
                            ,description.getText().toString().trim(),email,stars.getRating(),datum, starscount,
                            bewertungsuser, stars.getRating(),cutyoutubeurl(image.getText().toString().trim()));
                    Toast.makeText(ItemDetailEditActivity.this,"Eintrag erfolgreich erstellt",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent( ItemDetailEditActivity.this, MainActivity.class));
                }else
                {
                    Toast.makeText(ItemDetailEditActivity.this,"Sie haben nicht alle Felder ausgefüllt",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void myNewMovie(String title, String image, String description, String erfasser,
                            Float stars, String datum, int starscount, String bewertungsuser,
                            float starsaverage, String youtubeurl ){
        Model model = new Model(title,image,erfasser,description, datum , stars, starscount, bewertungsuser, starsaverage, youtubeurl);
        //mDatabaseReference.child(userID).push().setValue(model);
        mDatabaseReference.child("Data").push().setValue(model);
        //mDatabaseReference.child(userID).removeValue();
    }
    private boolean isEmpty(TextInputEditText textInputEditText){
        if(textInputEditText.getText().toString().trim().length() > 0 && stars.getRating()>= 0){
            return false;
        }
        return true;
    }
    public String cutyoutubestring (String imagepath){
         String videoId = imagepath.split("v=")[1];
         imagepath = "https://img.youtube.com/vi/"+videoId+"/maxresdefault.jpg";
        return imagepath;
    }
    public String cutyoutubeurl (String url){
        String videoId = url.split("v=")[1];
        return videoId;
    }
}
