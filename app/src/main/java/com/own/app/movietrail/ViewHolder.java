package com.own.app.movietrail;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso;

public class ViewHolder extends RecyclerView.ViewHolder {
    private TextView mTitleTv;
    private TextView mDetailTv;
    private ImageView mImageIv;
    private RatingBar mRatingTv;
    private TextView mDateTv;
    private TextView mErfasserTv;
    private TextView maverageTv;
    View mView;
    public ViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mTitleTv = mView.findViewById(R.id.rTitleTv);
        //mDetailTv = mView.findViewById(R.id.rDescription);
        mImageIv = mView.findViewById(R.id.rImageView);
        mRatingTv = mView.findViewById(R.id.ratingBar);
        mDateTv = mView.findViewById(R.id.generateDate);
        mErfasserTv = mView.findViewById(R.id.erfasser);
        maverageTv = mView.findViewById(R.id.textaverage);
    }
    public void setTitle (String title){ mTitleTv.setText(title); }
    public void setDescription (String description){ mDetailTv.setText(description); }
    public void setImage (String image){ Picasso.get().load(image).into(mImageIv); }
    public void setRating (float stars){ mRatingTv.setRating(stars); }
    public void setDatum (String datum){ mDateTv.setText(datum); }
    public void setErfasser (String erfasser){ mErfasserTv.setText(erfasser); }
    public void setMaverage (Float maverage){ String stramount = String.valueOf(maverage); maverageTv.setText(stramount); }
    public void bind (Model model){
        setTitle(model.getTitle());
        //setDescription(model.getDescription());
        setImage(model.getImage());
        setRating(model.getStarsaverage());
        setDatum(model.getDatum());
        setErfasser(model.getErfasser());
        setMaverage(model.getStarsaverage());
    }
}
