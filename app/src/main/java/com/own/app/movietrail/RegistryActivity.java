package com.own.app.movietrail;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistryActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private Button buttonregister;
    private EditText edittextemailregistry;
    private EditText edittextpasswordregistry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry);
        mAuth = FirebaseAuth.getInstance();
        buttonregister = (Button) findViewById(R.id.buttonregister);
        edittextemailregistry = (EditText) findViewById(R.id.editemailregister);
        edittextpasswordregistry = (EditText) findViewById(R.id.editpasswordregister);
        buttonregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edittextemailregistry.getText().toString().trim();
                String password = edittextpasswordregistry.getText().toString().trim();
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(RegistryActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()){
                                    Log.d("@@@", "User erfolgreich erstellt");
                                    Toast.makeText(RegistryActivity.this,"User erfolgreich erstellt",Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(RegistryActivity.this, LoginActivity.class));
                                }
                                else {
                                    Log.w("@@@","User konnte nicht erstellt werden", task.getException());
                                    Toast.makeText(RegistryActivity.this,"User konnte nicht erstellt werden",Toast.LENGTH_SHORT).show();

                                }
                            }
                        });
            }
        });
    }
}
