package com.own.app.movietrail;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Fragment;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.HashMap;

public class ItemDetailActivity extends AppCompatActivity  implements YouTubePlayer.OnInitializedListener {
    private ImageView mimageView;
    private TextView titledetail;
    private TextView datumdetail;
    private TextView erfasserdetail;
    private TextView descriptiondetail;
    private FirebaseDatabase mRef;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference usersRef;
    private String userID;
    private String positionspfad;
    private RatingBar rating;
    private Button btnbearbeiten;
    private Button btnloeschen;
    private Button btnspeichern;
    private static String VIDEO_ID = "";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    YouTubePlayerFragment myYouTubePlayerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        myYouTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager()
                .findFragmentById(R.id.youtubeview);
        myYouTubePlayerFragment.initialize(YouTubeConfig.getApiKey(), this);
        mAuth = FirebaseAuth.getInstance();
        mRef = FirebaseDatabase.getInstance();
        usersRef = FirebaseDatabase.getInstance().getReference();
        //Button zuweisen für löschen und bearbeiten
        btnbearbeiten = (Button) findViewById(R.id.eintragbearbeiten);
        btnspeichern = (Button) findViewById(R.id.btnspeichern);
        btnloeschen = (Button) findViewById(R.id.eintraglöschen);
        //Title von der Detailansicht
        String qty2 = getIntent().getStringExtra("mTitleTv");
        titledetail = findViewById(R.id.titeldetail);
        titledetail.setText(qty2);
        String qty3 = getIntent().getStringExtra("mDateTv");
        datumdetail = findViewById(R.id.datumdetail);
        datumdetail.setText(qty3);
        final String qty4 = getIntent().getStringExtra("mErfasserTv");
        erfasserdetail = findViewById(R.id.erfasserdetail);
        erfasserdetail.setText(qty4);
        final String qty5 = getIntent().getStringExtra("mDetailTv");
        descriptiondetail = findViewById(R.id.descriptiondetail);
        descriptiondetail.setText(qty5);
        descriptiondetail.setFocusable(false);
        //descriptiondetail.setInputType(InputType.TYPE_NULL);
        VIDEO_ID = getIntent().getStringExtra("myoutubeurl");
        final Integer qty6 = getIntent().getIntExtra("mstarscount", 0);
        final float qty7 = getIntent().getFloatExtra("mstars", 0);
        positionspfad = getIntent().getStringExtra("mclickpath");
        //Erfasser mit dem User vergleichen um die Button auszublenden
        FirebaseUser currentUser = mAuth.getCurrentUser();
        String email = currentUser.getEmail();
        if (!email.equals(qty4)) {
            btnbearbeiten.setVisibility(View.GONE);
            btnloeschen.setVisibility(View.GONE);
        }
        //Dieser RatingListener dient dazu um das Rating zu erfassen für den ausgewählten Film
        //Das Rating wird berechnet und in die Datenbank geschrieben.
        rating = findViewById(R.id.rating_bar);
        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                float sterne = ratingBar.getRating();
                Integer count = qty6 + 1;
                float starscount = qty7 + sterne;
                float average = starscount / count;
                float averageend = round(average);
                //Format auf eine Kommastelle begrenzen
                HashMap<String, Object> result = new HashMap<>();
                result.put("stars", starscount);
                usersRef.child("Data").child(positionspfad).updateChildren(result);
                result.put("starscount", count);
                usersRef.child("Data").child(positionspfad).updateChildren(result);
                result.put("starsaverage", averageend);
                usersRef.child("Data").child(positionspfad).updateChildren(result);
            }
        });
        //Buttonauslöser um die veränderung in die Datenbank zu schreiben
        btnspeichern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> result = new HashMap<>();
                String description = descriptiondetail.getText().toString();
                result.put("description", description);
                usersRef.child("Data").child(positionspfad).updateChildren(result);
                descriptiondetail.setFocusable(false);
                //descriptiondetail.setInputType(InputType.TYPE_NULL);
                v.setVisibility(View.GONE);
                View b = findViewById(R.id.eintragbearbeiten);
                b.setVisibility(View.VISIBLE);
            }
        });
        btnloeschen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usersRef.child("Data").child(positionspfad).removeValue();
                startActivity(new Intent(ItemDetailActivity.this, MainActivity.class));
            }
        });
        // buttonauslöser für die Description zum bearbeiten
        btnbearbeiten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                View b = findViewById(R.id.btnspeichern);
                b.setVisibility(View.VISIBLE);
                //descriptiondetail.setInputType(InputType.TYPE_CLASS_TEXT);
                descriptiondetail.setFocusableInTouchMode(true);
                descriptiondetail.setFocusable(true);
            }
        });
    }
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format("There was an error", youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo(VIDEO_ID);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            getYouTubePlayerProvider().initialize(YouTubeConfig.getApiKey(), this);
        }
    }
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtubeview);
    }
    //Rundungsfunktion um den Average auf oder abzurunden
    public float round(float input) {
        final float interval = 0.5f;
        final float halfInterval = 0.25f;
        float rest = input % interval;
        float divided = (int) (input / interval);
        if (0 == rest) {
            return input;
        }
        if (rest >= halfInterval) {
            return divided * interval + interval;
        }
        return divided * interval;
    }
}
