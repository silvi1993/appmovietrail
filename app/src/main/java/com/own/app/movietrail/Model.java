package com.own.app.movietrail;

import android.view.Display;

import retrofit2.http.PUT;

public class Model {

    //Declaration muss wie die Namen im Firebase sein
    String title, image, erfasser, description, datum , bwertungsuser,youtubeurl;
    float stars;
    int starscount;
    float starsaverage;

    public Model(String title, String image,
                 String erfasser, String description,
                 String datum, float stars, int starscount,
                 String bewertungsuser, float starsaverage,
                 String youtubeurl) {
        this.title = title;
        this.image = image;
        this.erfasser = erfasser;
        this.description = description;
        this.datum = datum;
        this.stars = stars;
        this.starscount = starscount;
        this.bwertungsuser = bewertungsuser;
        this.starsaverage = starsaverage;
        this.youtubeurl = youtubeurl;
    }
    public Model(){
    }
    public String getYoutubeurl() {
        return youtubeurl;
    }
    public void setYoutubeurl(String youtubeurl) {
        this.youtubeurl = youtubeurl;
    }
    public float getStarsaverage() {
        return starsaverage;
    }
    public void setStarsaverage(float starsaverage) {
        this.starsaverage = starsaverage;
    }
    public String getBwertungsuser() {
        return bwertungsuser;
    }
    public void setBwertungsuser(String bwertungsuser) {
        this.bwertungsuser = bwertungsuser;
    }
    public int getStarscount() {
        return starscount;
    }
    public void setStarscount(int starscount) {
        this.starscount = starscount;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getErfasser() {
        return erfasser;
    }
    public void setErfasser(String erfasser) {
        this.erfasser = erfasser;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDatum() {
        return datum;
    }
    public void setDatum(String datum) {
        this.datum = datum;
    }
    public float getStars() {
        return stars;
    }
    public void setStars(float stars) {
        this.stars = stars;
    }
}
