package com.own.app.movietrail;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    //Firebase Auth Variable
    private FirebaseAuth mauth;
    RecyclerView mRecyclerView;
    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference mRef;
    DatabaseReference mRef2;
    private FloatingActionButton fab;
    public String clickpath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Firebase Authentifizierung instanzieren
        mauth = FirebaseAuth.getInstance();
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
        //Befehl um das Layout umzukehren das die sortierung mit childkey funktioniert
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mFirebaseDatabase.getReference("Data");
        mRef2 = mFirebaseDatabase.getReference();
        fab = (FloatingActionButton) findViewById(R.id.float_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newentry = new Intent(getBaseContext(), ItemDetailEditActivity.class);
                startActivity(newentry);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        //Prüfen ob ein User angemeldet ist
        FirebaseUser currentUser = mauth.getCurrentUser();
        if(currentUser == null){
            //Activty wechseln zu Login Activity
            Intent loginIntent = new Intent(getBaseContext(), LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }
        this.attachRecyclerViewAdapter();
    }
    private void attachRecyclerViewAdapter(){
        final RecyclerView.Adapter adapter = newAdapter();
        mRecyclerView.setAdapter(adapter);
    }
    protected RecyclerView.Adapter newAdapter () {
        FirebaseRecyclerOptions<Model> options =
                new FirebaseRecyclerOptions.Builder<Model>()
                        .setQuery(mRef.orderByChild("datum"), Model.class)
                        .setLifecycleOwner(this)
                        .build();
        return new FirebaseRecyclerAdapter<Model, ViewHolder>(options) {
            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                return new ViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_main, parent, false));
            }
            @Override
            protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final Model model){
                holder.bind(model);
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(MainActivity.this, ItemDetailActivity.class);
                        Toast.makeText(MainActivity.this, "Aktuelle Position: " + model.getTitle(),Toast.LENGTH_SHORT).show();
                        mRef2 = getRef(position);
                        clickpath = mRef2.getKey();
                        i.putExtra("mImageTv", model.getImage());
                        i.putExtra("mTitleTv",model.getTitle());
                        i.putExtra("mDateTv",model.getDatum());
                        i.putExtra("mErfasserTv",model.getErfasser());
                        i.putExtra("mDetailTv",model.getDescription());
                        i.putExtra("mclickpath", clickpath);
                        i.putExtra("mstarscount",model.getStarscount());
                        i.putExtra("mstars",model.getStars());
                        i.putExtra("myoutubeurl", model.getYoutubeurl());
                        startActivity(i);
                    }
                });
            }
        };
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Menu auf der MainActivity erstellen
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.signout:
                //User ausloggen
                mauth.signOut();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                return true;
            case R.id.about:
                startActivity(new Intent(MainActivity.this, AboutActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
